# Date: 15 July 2017
#  Font Awesome Integration
## Resources
- [`Font Awesome`](http://fontawesome.io/)
- [`Font-Awesome`](https://github.com/FortAwesome/Font-Awesome)
- [`font-awesome-loader`](https://github.com/shakacode/font-awesome-loader)
- [`react-fontawesome`](https://github.com/danawoodman/react-fontawesome)
## Steps:
1. Install [`Font-Awesome`](https://github.com/FortAwesome/Font-Awesome)
```
npm install --save font-awesome
```
or
```
yarn add font-awesome
```
2. Install [`font-awesome-loader`](https://github.com/shakacode/font-awesome-loader) and [`react-fontawesome`](https://github.com/danawoodman/react-fontawesome)  
```
yarn add -D font-awesome-loader eact-fontawesome
```
or
```
npm install --save-dev font-awesome-loader eact-fontawesome
```
3. Create two Font Awesome Webpack custom config files:  
refer to [`font-awesome-loader`](https://github.com/shakacode/font-awesome-loader/blob/master/docs/usage-webpack2.md#custom-configuration)  

*`font-awesome.config.js`*
```js
module.exports = {
  styles: {
    'mixins': true,
    'bordered-pulled': true,
    'core': true,
    'fixed-width': true,
    'icons': true,
    'larger': true,
    'list': true,
    'path': true,
    'rotated-flipped': true,
    'animated': true,
    'stacked': true
  }
};
```

*`font-awesome.config.scss`*  
Using this file to override scss variables for `Font Awesome`
```scss
// $fa-font-path:  "./resources/font-awesome/fonts";

$fa-inverse: #eee;
$fa-border-color: #ddd;
```

4. Add `font-awesome-loader` configuration for `font-awesome.config.js` to `webpack.config.base.js`  
```js
module.exports = {
  module: {
    loaders: [
      ...,
      {
        test: /font-awesome\.config\.js/,
        use: [
          {
            loader: "style-loader"
          }, {
            loader: "font-awesome-loader"
          }
        ]
      }
      ...
    ]
  },

...
};

```

5. Complete Font Awesome  
Refer to [`font-awesome-loader`](https://github.com/shakacode/font-awesome-loader/blob/master/docs/usage-webpack2.md#complete-font-awesome)  

Adding `Font Awesome` scss file and the custom `font-awesome.config.js` to the entry section in `webpack.config.development.js` as below:
```js
module.exports = {
  entry: [
    ...
    'font-awesome/scss/font-awesome.scss',
    './font-awesome.config',
    ...
  ]
};
```

