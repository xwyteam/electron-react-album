import * as React from 'react';

import Navbar from './Navbar';
import Workspace from './Workspace';
import Sidebar from './Sidebar';

const styles = require('./Home.scss');

export default class Home extends React.Component {
  render() {
    return (
      <div className="layout">
        <div className={styles.container} data-tid="container">
          <Navbar/>
          <Sidebar/>
          <Workspace/>
        </div>
      </div>
    );
  }
}
