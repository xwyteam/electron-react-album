import * as React from 'react';

import {Route} from 'react-router-dom';

const styles = require('./Workspace.scss');

export default class Workspace extends React.Component {
    static routes = [
        {
            path: '/',
            exact: true,
            main: () => <h2>Dashboard</h2>
        }, {
            path: '/dashboard',
            exact: false,
            main: () => <h2>Dashboard</h2>
        }, {
            path: '/photos',
            exact: false,
            main: () => <h2>Photos</h2>
        }, {
            path: '/albums',
            exact: false,
            main: () => <h2>Albums</h2>
        }, {
            path: '/tags',
            exact: false,
            main: () => <h2>Tags</h2>
        }, {
            path: '/trash',
            exact: false,
            main: () => <h2>Trash</h2>
        }
    ]

    render() {
        return (
            <div className={styles.container}>
                {
                    Workspace.routes.map((route, index) => (
                        <Route key={index} path={route.path} exact={route.exact} component={route.main}></Route>
                    ))
                }
            </div>
        );
    }
}
