import * as React from 'react';
var FA = require('react-fontawesome');

import {Link} from 'react-router-dom'

const styles = require('./Sidebar.scss');

export default class Sidebar extends React.Component {
    render() {
        return (
            <div className={styles.container}>
                <div>
                    <Link to="/dashboard">
                        <FA name="bar-chart"></FA>
                    </Link>
                </div>

                <div>
                    <Link to="/albums">
                        <FA name="camera"></FA>
                    </Link>
                </div>

                <div>
                    <Link to="/photos"><FA name='picture-o'/></Link>
                </div>

                <div>
                    <Link to="/tags"><FA name='tags'/></Link>
                </div>

                <div>
                    <Link to="/trash"><FA name='trash'/></Link>
                </div>
            </div>

        );
    }
}